package br.com.effortech;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class ApiEffortech {
    public static String getMessage(String apiKey, int maxResult, Calendar dateInitial, Calendar dateFinal){
        String url = "http://api.syrinxiot.com:7474/syrinx-alpha/api/restricted/exdom/message?field=all&maxResult=" + maxResult + "&dateGE=" + util.DateUnixTime(dateInitial) + "&dateLT=" + util.DateUnixTime(dateFinal);
        System.out.println(url);
        String method = "GET";

        try {
            URL apiEnd = new URL(url + "&apiKey=" + apiKey);            
            HttpURLConnection conn = (HttpURLConnection) apiEnd.openConnection();            
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod(method);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setUseCaches(false);
            conn.connect();
            InputStream is;
            
            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                is = conn.getInputStream();
            } else {
                is = conn.getErrorStream();
            }

            String strJson = ConverterInputStreamToString(is);
            
            is.close();
            conn.disconnect();

            return strJson;

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public static String sendMessage(String apiKey, String data){
        String url = "http://api.syrinxiot.com:7474/syrinx-alpha/api/restricted/deviceterminatedmessage";
        String method = "POST";
        
        try {
            URL apiEnd = new URL(url + "?apiKey=" + apiKey);            
            HttpURLConnection conn = (HttpURLConnection) apiEnd.openConnection();            
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod(method);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setUseCaches(false);

            conn.setDoOutput(true);
            conn.getOutputStream().write(data.getBytes());
            
            conn.connect();
            InputStream is;
            
            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                is = conn.getInputStream();
            } else {
                is = conn.getErrorStream();
            }

            String strJson = ConverterInputStreamToString(is);
            
            is.close();
            conn.disconnect();

            return strJson;

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    
    private static String ConverterInputStreamToString(InputStream is) {
        StringBuffer buffer = new StringBuffer();
        try {
            BufferedReader br;
            String linha;

            br = new BufferedReader(new InputStreamReader(is));
            while ((linha = br.readLine()) != null) {
                buffer.append(linha);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }
}
