package br.com.effortech;

import java.util.Calendar;

public class util {
  public static String hexToString(byte[] ba) {
    StringBuilder str = new StringBuilder();
    for (int i = 0; i < ba.length; i++)
      str.append(String.format("%x", ba[i]));
    return str.toString();
  }

  public static String hexToString(String hex) {
    StringBuilder str = new StringBuilder();
    for (int i = 0; i < hex.length(); i += 2) {
      str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
    }
    return str.toString();
  }

  public static String stringToHex(String string) {
    StringBuilder buf = new StringBuilder(200);
    for (char ch : string.toCharArray()) {
      if (buf.length() > 0)
        buf.append(' ');
      buf.append(String.format("%04x", (int) ch));
    }
    return buf.toString();
  }

  public static long DateUnixTime(){
    long unixTime = System.currentTimeMillis();

    return unixTime;
  }

  public static long DateUnixTime(Calendar calendar){
    long unixTime = calendar.getTimeInMillis();

    return unixTime;
  }

  
}
