package br.com.effortech;

import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

public class App {
    public static void main(String[] args) {
        System.out.println("Effortech - Consumir API Syrinx");
        String apiKey = "1234567890";
        int maxResult = 100;
        Calendar dateInitial = Calendar.getInstance();
        Calendar dateFinal = Calendar.getInstance();
        dateInitial.set(2021, 03, 01, 0, 0, 0);
        dateFinal.set(2021, 04, 19, 23, 59, 59);

        String strJson = ApiEffortech.getMessage(apiKey, maxResult, dateInitial, dateFinal);
        JSONObject messageJson = new JSONObject(strJson);
        JSONArray messageArray = messageJson.getJSONArray("items");

        for (int i = 0; i < messageArray.length(); i++) {
            JSONObject message = messageArray.getJSONObject(i);
            // System.out.println("MSG " + i + ": " + message.getString("protocol"));

            String strProtocol = message.getString("protocol");
            JSONObject protocol = new JSONObject(message.getString("protocol"));

            // Mensagem Leaf
            if (strProtocol.contains("content")) {
                System.out.println("Leaf " + i + ": " + protocol.getString("content"));
                System.out.println("Conversão do Hexa (Leaf) " + i + ": " + util.hexToString(protocol.getString("content")));
            }

            // Mensagem em modo transparente
            if (strProtocol.contains("data")) {
                System.out.println("MSG Transparente " + i + ": " + protocol.getString("data"));
                // System.out.println("Conversão do Hexa (MSG Transparente) " + i + ": " +
                // util.hexToString(protocol.getString("data")));
            }
        }

        // Enviando mensagem

        JSONObject msgContent = new JSONObject();
        msgContent.put("type", "ASCII");
        msgContent.put("data", "Teste de envio de mensagem");

        JSONObject msgJSON = new JSONObject();
        msgJSON.put("deviceIdentification", "300234067217040");
        msgJSON.putOnce("content", msgContent.toString());
        msgJSON.put("creationDate", util.DateUnixTime());

        System.out.println("Enviando mensagem: " + msgJSON.toString());
        ApiEffortech.sendMessage(apiKey, msgJSON.toString());
        System.out.println("Mensagem enviada.");
    }

}
